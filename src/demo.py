import streamlit as st
from typing import Dict
import yaml
import torch
from model.unet_segment_model import (
    UnetSegmentModel
)
from PIL import Image
import numpy as np
from data_module.utils import preprocess_image
from torch import Tensor
import random


st.set_page_config(
    page_title="Car segment Unet demo",
    layout="centered"
)


st.title("Car segment Unet demo")
st.markdown("---")


@st.cache_resource
def load_config() -> Dict:
    """
    Load config
    :return: config
    """
    with open("configs/predict_config.yaml", mode="r") as file_obj:
        result: Dict = yaml.safe_load(file_obj)
    return result


config: Dict = load_config()
device = torch.device(config["device"])


@st.cache_resource
def load_model() -> UnetSegmentModel:
    """
    Load model
    """
    result = UnetSegmentModel.load_from_checkpoint(
        checkpoint_path=config["checkpoint_path"]
    ).to(device)
    result.eval()
    return result


model = load_model()


uploaded_file = st.file_uploader("Choose an image")
st.markdown("---")


if uploaded_file is not None:
    pil_image = Image.open(uploaded_file).convert('RGB')

    with st.spinner(text="Processing..."):
        image: np.ndarray = preprocess_image(
            pil_image=pil_image, scale=config["scale"]
        )

        image: Tensor = torch.tensor(image, dtype=torch.float)
        with torch.no_grad():
            image: Tensor = image.unsqueeze(dim=0)

            model.eval()
            mask: Tensor = model.predict_step(
                batch={
                    "image": image.to(device)
                },
                batch_idx=0, dataloader_idx=0
            )
            mask: Tensor = mask.squeeze(dim=0)
            mask: np.ndarray = mask.cpu().detach().numpy()

        mask_rgb = np.zeros(
            shape=(mask.shape[0], mask.shape[1], 3), dtype=mask.dtype
        )
        for value in np.unique(mask):
            mask_rgb[mask == value] = [
                random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
            ]

    st.subheader("Uploaded image")
    with st.columns(3)[1]:
        st.image(image=pil_image)
    st.markdown("---")

    st.subheader("Predict mask")
    with st.columns(3)[1]:
        st.image(image=mask_rgb)

