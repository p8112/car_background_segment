import yaml
from typing import Dict, List, Tuple
import os
from sklearn.model_selection import train_test_split
from pathlib import Path
import pickle
from data_module.utils import load_image
import numpy as np
from tqdm import tqdm


with open("configs/process_data_config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)

images_dir: str = config["images_dir"]
masks_dir: str = config["masks_dir"]

image_files_mask_files: List[Tuple[str, str]] = []
for image_file in os.listdir(images_dir):
    mask_file: str = os.path.splitext(image_file)[0]
    mask_file: str = f"{mask_file}_mask.gif"
    image_file: str = images_dir + os.path.sep + image_file
    mask_file: str = masks_dir + os.path.sep + mask_file
    if (
        os.path.isfile(image_file) and
        os.path.isfile(mask_file)
    ):
        image_files_mask_files.append(
            (image_file, mask_file)
        )

train_image_files_mask_files, val_image_files_mask_files = train_test_split(
    image_files_mask_files, train_size=config["train_size"]
)
val_image_files_mask_files, test_image_files_mask_files = train_test_split(
    val_image_files_mask_files,
    train_size=config["val_size"] / (1 - config["train_size"])
)
print(
    f"Train size: {len(train_image_files_mask_files)}",
    f"Validation size: {len(val_image_files_mask_files)}",
    f"Test size: {len(test_image_files_mask_files)}",
    sep="\t"
)

Path(config["annotate_dir"]).mkdir(parents=True, exist_ok=True)
with open(
    f"{config['annotate_dir']}/train.pkl", mode="wb"
) as file_obj:
    pickle.dump(train_image_files_mask_files, file_obj)
with open(
    f"{config['annotate_dir']}/val.pkl", mode="wb"
) as file_obj:
    pickle.dump(val_image_files_mask_files, file_obj)
with open(
    f"{config['annotate_dir']}/test.pkl", mode="wb"
) as file_obj:
    pickle.dump(test_image_files_mask_files, file_obj)


mask_values: List = []
progress_bar = tqdm(
    iterable=image_files_mask_files, desc="Get mask values"
)
for _, mask_file in progress_bar:
    pil_image = load_image(filename=mask_file)
    image = np.asarray(pil_image)
    mask_values.append(np.unique(image))
progress_bar.close()
mask_values: np.ndarray = np.concatenate(mask_values, axis=0)
mask_values: np.ndarray = np.unique(mask_values)
mask_values: List = sorted(mask_values.tolist())
print(f"Number of classes: {len(mask_values)}")

with open(
    f"{config['annotate_dir']}/mask_values.pkl", mode="wb"
) as file_obj:
    pickle.dump(mask_values, file_obj)
