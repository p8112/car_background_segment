"""
All sub-modules of Unet model
"""

import torch
import torch.nn as nn
import torch.nn.functional as torch_func
from typing import Optional
from torch import Tensor


class DoubleConv(nn.Module):
    """
    - Conv; Batch norm; Relu
    - Conv; Batch norm; Relu
    Keep spacial size
    """

    def __init__(
            self, in_channels: int, out_channels: int,
            mid_channels: Optional[int] = None
    ):
        """
        Init method
        :param in_channels: number channels of input feature maps
        :param out_channels: number channels of output feature maps
        :param mid_channels: number channels of intermediate feature maps
        """
        super().__init__()

        if not mid_channels:
            mid_channels = out_channels

        self.double_conv = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels, out_channels=mid_channels,
                kernel_size=(3, 3), padding=1, bias=False
            ),
            nn.BatchNorm2d(num_features=mid_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=mid_channels, out_channels=out_channels,
                kernel_size=(3, 3), padding=1, bias=False
            ),
            nn.BatchNorm2d(num_features=out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute feature maps
        :param x: batch size, in_channels, height, width
        :return: batch size, out_channels, height, width
        """
        return self.double_conv(x)


class Down(nn.Module):
    """
    Decrease spacial size by half using max pooling
    Then using double conv
    """

    def __init__(
            self, in_channels: int, out_channels: int
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param out_channels: number channels of output
        """
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(kernel_size=2),
            DoubleConv(in_channels=in_channels, out_channels=out_channels)
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute feature maps
        :param x: batch size, in_channels, height, width
        :return: batch size, out_channels, height // 2, width // 2
        """
        return self.maxpool_conv(x)


class Up(nn.Module):
    """
    Increase spacial size by double using up-sample layer or conv-transpose layer
    Then using double-conv
    """

    def __init__(
            self, in_channels: int, out_channels: int, bilinear: bool = True
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param out_channels: number channels of output
        :param bilinear: use bilinear layer or conv-transpose
        """
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = nn.Upsample(
                scale_factor=2, mode='bilinear', align_corners=True
            )
            self.conv = DoubleConv(
                in_channels=in_channels, out_channels=out_channels,
                mid_channels=in_channels // 2
            )
        else:
            self.up = nn.ConvTranspose2d(
                in_channels=in_channels, out_channels=in_channels // 2,
                kernel_size=(2, 2), stride=(2, 2)
            )
            self.conv = DoubleConv(
                in_channels=in_channels, out_channels=out_channels
            )

    def forward(self, x1: Tensor, x2: Tensor) -> Tensor:
        """
        Compute feature map
        :param x1: feature map from lower expansive path
            if bilinear:
                batch size, in_channels / 2, height // 2, width // 2
            else:
                batch size, in_channels, height // 2, width // 2
        :param x2: feature map from corresponding contracting path
            batch size, in_channels / 2, height, width
        :return:  batch size, out_channels, height, width
        """
        x1 = self.up(x1)    # batch size, in channels, height, width

        diff_height: int = x2.shape[2] - x1.shape[2]
        diff_width: int = x2.shape[3] - x1.shape[3]

        x1 = torch_func.pad(
            x1,
            [
                diff_width // 2, diff_width - diff_width // 2,
                diff_height // 2, diff_height - diff_height // 2
            ]
        )   # batch size, in channels / 2, height, width

        x = torch.cat([x2, x1], dim=1)    # batch size, in channels, height, width
        return self.conv(x)    # batch size, out channels, height, width


class OutConv(nn.Module):
    """
    Conv layer to compute output feature maps
    Keep spacial size
    """
    def __init__(
            self, in_channels: int, out_channels: int
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param out_channels: number channels of output
        """
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(
            in_channels=in_channels, out_channels=out_channels, kernel_size=(1, 1)
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute feature maps
        :param x: batch size, in_channels, height, width
        :return: batch size, out_channels, height, width
        """
        return self.conv(x)
