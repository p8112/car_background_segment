"""
Full model model
"""

from .unet_parts import (
    Up, Down, OutConv, DoubleConv
)
import torch.nn as nn
from torch import Tensor


class UNet(nn.Module):
    """
    Full model model
    Spacial size does not change after model
    """
    def __init__(
            self, in_channels: int, out_channels: int, bilinear: bool = False
    ):
        """
        Init method
        :param in_channels: number channels of input image
        :param out_channels: number channels of output feature maps
        :param bilinear: using bilinear (instead of conv-transpose) in up layer
        """
        super(UNet, self).__init__()
        self.in_channels: int = in_channels
        self.out_channels: int = out_channels
        self.bilinear: bool = bilinear

        factor: int = 2 if self.bilinear else 1

        self.inc = DoubleConv(in_channels=in_channels, out_channels=64)

        self.down1 = Down(in_channels=64, out_channels=128)
        self.down2 = Down(in_channels=128, out_channels=256)
        self.down3 = Down(in_channels=256, out_channels=512)
        self.down4 = Down(in_channels=512, out_channels=1024 // factor)

        self.up1 = Up(in_channels=1024, out_channels=512 // factor, bilinear=bilinear)
        self.up2 = Up(in_channels=512, out_channels=256 // factor, bilinear=bilinear)
        self.up3 = Up(in_channels=256, out_channels=128 // factor, bilinear=bilinear)
        self.up4 = Up(in_channels=128, out_channels=64, bilinear=bilinear)

        self.out_conv = OutConv(in_channels=64, out_channels=out_channels)

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute output feature maps
        :param x: batch size, in_channels, height, width
        :return: batch size, out_channels, height, width
        """
        x1 = self.inc(x)

        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)

        out = self.up1(x5, x4)
        out = self.up2(out, x3)
        out = self.up3(out, x2)
        out = self.up4(out, x1)

        out = self.out_conv(out)
        return out
