from pytorch_lightning import LightningModule
from .unet_model import UNet
import torch.nn as nn
from torch import Tensor
from typing import Dict
from torch.nn.functional import softmax, one_hot
import torch
from torchmetrics import Accuracy, F1Score
from torch.optim import RMSprop
from torch.optim.lr_scheduler import ReduceLROnPlateau


class UnetSegmentModel(LightningModule):
    """
    Using Unet model for segmentation problem
    """
    def __init__(
            self, in_channels: int, num_classes: int,
            learning_rate: float, weight_decay: float, momentum: float
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param num_classes: number classes in segmented image
        :param learning_rate: learning rate of model
        :param weight_decay: weight decay of model
        :param momentum: momentum of model
        """
        super(UnetSegmentModel, self).__init__()
        self.save_hyperparameters()
        self.in_channels: int = in_channels
        self.num_classes: int = num_classes

        self.model = UNet(in_channels=in_channels, out_channels=num_classes)
        self.loss_fn = nn.CrossEntropyLoss()
        self.accuracy_fn = Accuracy(
            task="multiclass", num_classes=self.num_classes
        )
        self.f1_fn = F1Score(
            task="multiclass", num_classes=self.num_classes
        )
        self.learning_rate: float = learning_rate
        self.weight_decay: float = weight_decay
        self.momentum: float = momentum

    def compute_dice_score(
            self, predict: Tensor, target: Tensor
    ) -> Tensor:
        """
        Compute dice score
        :param predict: batch size, num_classes, height, width
        :param target: batch size, height, width
        :return: scalar
        """
        predict = softmax(predict, dim=1)    # batch size, num_classes, height, width
        target = one_hot(
            target, num_classes=self.num_classes
        ).permute(0, 3, 1, 2).float()     # batch size, num_classes, height, width

        predict = predict.flatten(0, 1)    # batch size * num_classes, height, width
        target = target.flatten(0, 1)     # batch size * num_classes, height, width

        inter = 2 * (predict * target).sum(dim=(1, 2))    # batch size
        sets_sum = predict.sum(dim=(1, 2)) + target.sum(dim=(1, 2))    # batch size
        sets_sum = torch.where(sets_sum == 0, inter, sets_sum)    # batch size

        dice = (inter + 1e-6) / (sets_sum + 1e-6)
        return dice.mean()

    def training_step(self, batch: Dict, *args, **kwargs) -> Tensor:
        """
        Compute training loss
        :param batch: dict contains
            - image: batch size, height, width, in channels
            - target: batch size, height, width
        """
        predict = self.model(x=batch["image"])   # batch size, num classes, height, width

        ent_loss = self.loss_fn(predict, batch["target"])
        self.log(
            name="train_ent_loss", value=ent_loss, prog_bar=True
        )

        dice_loss = 1 - self.compute_dice_score(
            predict=predict, target=batch["target"]
        )
        self.log(
            name="train_dice_loss", value=dice_loss, prog_bar=True
        )

        loss = ent_loss + dice_loss
        self.log(
            name="train_loss", value=loss, prog_bar=True
        )
        return loss

    def validation_step(self, batch: Dict, *args, **kwargs):
        """
        Compute metrics
        :param batch: dict contains
            - image: batch size, height, width, in channels
            - target: batch size, height, width
        """
        predict = self.model(x=batch["image"])  # batch size, num classes, height, width

        dice_score = self.compute_dice_score(
            predict=predict, target=batch["target"]
        )
        self.log(
            name="val_dice_score", value=dice_score, prog_bar=True
        )

        predict = softmax(predict, dim=1)    # batch size, num_classes, height, width

        accuracy = self.accuracy_fn(predict, batch["target"])
        self.log(
            name="val_accuracy", value=accuracy, prog_bar=True
        )

        f1 = self.f1_fn(predict, batch["target"])
        self.log(
            name="val_f1", value=f1, prog_bar=True
        )

    def test_step(self, batch: Dict, *args, **kwargs):
        """
        Compute metrics
        :param batch: dict contains
            - image: batch size, height, width, in channels
            - target: batch size, height, width
        """
        predict = self.model(x=batch["image"])  # batch size, num classes, height, width

        dice_score = self.compute_dice_score(
            predict=predict, target=batch["target"]
        )
        self.log(
            name="test_dice_score", value=dice_score, prog_bar=True
        )

        predict = softmax(predict, dim=1)    # batch size, num_classes, height, width

        accuracy = self.accuracy_fn(predict, batch["target"])
        self.log(
            name="test_accuracy", value=accuracy, prog_bar=True
        )

        f1 = self.f1_fn(predict, batch["target"])
        self.log(
            name="test_f1", value=f1, prog_bar=True
        )

    def predict_step(
            self, batch: Dict, batch_idx: int, dataloader_idx: int = 0
    ) -> Tensor:
        """
        Compute predict
        :param batch: dict contains
            - image: batch size, height, width, in channels
            - target: batch size, height, width
        :param batch_idx
        :param dataloader_idx
        """
        predict = self.model(x=batch["image"])  # batch size, num classes, height, width
        predict = torch.argmax(predict, dim=1, keepdim=False)    # batch size, height, width
        return predict

    def configure_optimizers(self) -> Dict:
        """
        Configure optimizer and scheduler
        """
        optimizer = RMSprop(
            params=self.parameters(), lr=self.learning_rate,
            weight_decay=self.weight_decay, momentum=self.momentum
        )
        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": ReduceLROnPlateau(
                    optimizer=optimizer, mode='max', patience=5
                ),
                "interval": "epoch",
                "strict": True,
                "monitor": "val_accuracy",
                "frequency": 1
            },
        }
