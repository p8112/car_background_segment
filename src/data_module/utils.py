from PIL import Image
from os.path import splitext
import numpy as np
import torch
from typing import List


def load_image(filename: str):
    """
    Load an image from file
    :return: PIL.Image object
    """
    ext: str = splitext(filename)[1]
    if ext == '.npy':
        return Image.fromarray(np.load(filename))
    elif ext in ['.pt', '.pth']:
        return Image.fromarray(torch.load(filename).numpy())
    else:
        return Image.open(filename)


def preprocess_image(
        pil_image, scale: float
) -> np.ndarray:
    """
    Preprocess image
    :param pil_image: PIL image object
    :param scale: image scaling
    :return: channels, height, width
    """
    width, height = pil_image.size
    new_width, new_height = int(scale * width), int(scale * height)

    pil_image = pil_image.resize(
        (new_width, new_height), resample=Image.BICUBIC
    )

    image: np.ndarray = np.asarray(pil_image)
    image = image.transpose((2, 0, 1))
    image = image / 255.0

    return image


def preprocess_mask(
        mask_values: List, pil_image, scale: float
) -> np.ndarray:
    """
    Preprocess image
    :param mask_values: list unique values of pixels in masks
    :param pil_image: PIL image object
    :param scale: image scaling
    :return: channels, height, width
    """
    width, height = pil_image.size
    new_width, new_height = int(scale * width), int(scale * height)

    pil_image = pil_image.resize(
        (new_width, new_height), resample=Image.NEAREST
    )

    image: np.ndarray = np.asarray(pil_image)
    mask: np.ndarray = np.zeros((new_height, new_width), dtype=np.int64)
    for index, value in enumerate(mask_values):
        mask[image == value] = index

    return mask
