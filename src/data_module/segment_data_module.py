from pytorch_lightning import LightningDataModule
from .segment_dataset import SegmentDataset
import pickle
from typing import List, Tuple
from torch.utils.data import DataLoader


class SegmentDataModule(LightningDataModule):
    """
    Data module for segment
    """
    def __init__(
            self, annotate_dir: str, scale: float,
            batch_size: int, num_workers: int
    ):
        """
        Init method
        :param annotate_dir: annotate directory
        :param scale: scale of image
        :param batch_size: batch size of training
        :param num_workers: number of workers
        """
        super(SegmentDataModule, self).__init__()
        self.save_hyperparameters()

        self.annotate_dir: str = annotate_dir
        self.scale: float = scale
        self.batch_size: int = batch_size
        self.num_workers: int = num_workers

        with open(
                f"{self.annotate_dir}/mask_values.pkl", mode="rb"
        ) as file_obj:
            self.mask_values: List = pickle.load(file_obj)

    def train_dataloader(self) -> DataLoader:
        """
        Training dataloader
        """
        with open(
                f"{self.annotate_dir}/train.pkl", mode="rb"
        ) as file_obj:
            image_files_mask_files: List[Tuple[str, str]] = pickle.load(file_obj)
        dataset = SegmentDataset(
            image_files_mask_files=image_files_mask_files,
            scale=self.scale, mask_values=self.mask_values
        )
        return DataLoader(
            dataset=dataset, batch_size=self.batch_size,
            shuffle=True, num_workers=self.num_workers
        )

    def val_dataloader(self) -> DataLoader:
        """
        Val dataloader
        """
        with open(
                f"{self.annotate_dir}/val.pkl", mode="rb"
        ) as file_obj:
            image_files_mask_files: List[Tuple[str, str]] = pickle.load(file_obj)
        dataset = SegmentDataset(
            image_files_mask_files=image_files_mask_files,
            scale=self.scale, mask_values=self.mask_values
        )
        return DataLoader(
            dataset=dataset, batch_size=self.batch_size,
            shuffle=False, num_workers=self.num_workers
        )

    def test_dataloader(self) -> DataLoader:
        """
        Test dataloader
        """
        with open(
                f"{self.annotate_dir}/test.pkl", mode="rb"
        ) as file_obj:
            image_files_mask_files: List[Tuple[str, str]] = pickle.load(file_obj)
        dataset = SegmentDataset(
            image_files_mask_files=image_files_mask_files,
            scale=self.scale, mask_values=self.mask_values
        )
        return DataLoader(
            dataset=dataset, batch_size=self.batch_size,
            shuffle=False, num_workers=self.num_workers
        )
