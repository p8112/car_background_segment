from .utils import (
    load_image, preprocess_image, preprocess_mask
)
from torch.utils.data import Dataset
from typing import List, Tuple, Dict
import torch
from torch import Tensor
import numpy as np


class SegmentDataset(Dataset):
    """
    Dataset for segment
    """
    def __init__(
            self, image_files_mask_files: List[Tuple[str, str]],
            scale: float, mask_values: List
    ):
        """
        Init method
        :param image_files_mask_files: list of image - mask file
        :param scale: scale of image
        :param mask_values: list of unique pixels value in mask
        """
        super(SegmentDataset, self).__init__()
        self.image_files_mask_files: List[Tuple[str, str]] = image_files_mask_files
        self.scale: float = scale
        self.mask_values: List = mask_values

    def __len__(self) -> int:
        return len(self.image_files_mask_files)

    def __getitem__(self, index: int) -> Dict[str, Tensor]:
        """
        Get a data instance
        :return: dict contain
            - image
            - target
        """
        image_file, mask_file = self.image_files_mask_files[index]
        image: np.ndarray = preprocess_image(
            pil_image=load_image(filename=image_file),
            scale=self.scale
        )
        mask: np.ndarray = preprocess_mask(
            mask_values=self.mask_values,
            pil_image=load_image(filename=mask_file),
            scale=self.scale
        )
        return {
            "image": torch.tensor(image, dtype=torch.float),
            "target": torch.tensor(mask, dtype=torch.long)
        }
